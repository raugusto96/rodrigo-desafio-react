import React, { useContext, useEffect, useState } from 'react'
import Button from '../../components/Button';
import UserContext from './../../utils/context/UserContext';
import { HiUsers, HiOutlineMail, HiLink, HiLocationMarker } from 'react-icons/hi';
import { BiBookBookmark } from 'react-icons/bi';
import { BsDot, BsGithub } from 'react-icons/bs';
import Card from './../../components/Card/index';
import api from '../../api';
import './index.scss';
import { useNavigate } from 'react-router-dom';
import storage from '../../utils/functions/localStorage';

const Profile = () => {
  const [isLoading, setLoading] = useState(true);
  const [repositories, setRepositories] = useState([]);
  const { user, setUser } = useContext(UserContext);
  let navigate = useNavigate();

  const fetchRepos = async () => {
    try {
      const recoveryUser = storage.recoveryItem('user');
      const repositories = await api.getRepositoriesData(recoveryUser.login);
      storage.saveItem('repositories', repositories);
      setRepositories(repositories);
      setLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchRepos();
    setUser(storage.recoveryItem('user'));
  }, []);

  if (isLoading) return (<p>Carregando...</p>);
  return (
    <main className='main-content-container'>
      <section>
        <div className='profile-container'>
          <img src={ user.avatar_url } alt="profile-image" />
          <div className='title-container'>
            <h2>{ user.name }</h2>
            <h3>{ user.login }</h3>
          </div>
          <div className='logo-container'>
            { <BsGithub onClick={ () => {
              navigate("/", { replace: true });
              storage.clearStorage();
          } } /> }
          </div>
        </div>
        <Button
          text="Follow"
          />
        <p>{ user.bio }</p>
        <div className='followers-container'>
          { <HiUsers /> } <span> { user.followers } followers</span>
          { <BsDot /> } <span> { user.following } following</span>
        </div>
        <div className='person-infos-container'>
          <span>
            { user.location && (<>{ <HiLocationMarker /> } { user.location }</>) }
          </span>
          <span>
            { <HiOutlineMail /> } { `${user.login}@gmail.com` }
          </span>
          <span>
            { <HiLink /> } { user.blog }
          </span>
        </div>
      </section>
      <section>
        <div>
          <nav>
            <ul>
              <li>{ <BiBookBookmark /> } Repositories <span className='counter'>{ user.public_repos }</span></li>
            </ul>
          </nav>
        </div>
        {
          repositories?.map((repositorie) => (
            <Card
              key={ repositorie.id }
              owner={ repositorie.owner.login }
              title={ repositorie.name }
              language={ repositorie.language }
              updated={ repositorie.updated_at }
              bio={ repositorie.description }
              isForked={ repositorie.fork }
              forks={ repositorie.forks }
              license={ repositorie.license }
            />
          ))
        }
      </section>
    </main>
  )
}

export default Profile;