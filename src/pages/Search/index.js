import React, { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { GoSearch } from 'react-icons/go';
import Label from '../../components/Label';
import Button from './../../components/Button/index';
import api from '../../api';
import UserContext from './../../utils/context/UserContext';
import errorsMessages from '../../utils/constants/errorMessages';
import sendNotify from '../../utils/functions/notify';
import './index.scss';
import storage from '../../utils/functions/localStorage';

const Search = () => {
  const [username, setUsername] = useState('');
  const { setUser } = useContext(UserContext);
  let navigate = useNavigate();

  const handleClick = async () => {
    try {
      if(username.length === 0) {
        sendNotify(errorsMessages.usernameNotDefined);
      } else {
        const user = await api.getUserData(username);
        setUser(user);
        storage.saveItem('user', user);
        navigate("/profile", { replace: true });
      }
    } catch (error) {
      if (error.response.data.message === 'Not Found') {
        sendNotify(errorsMessages.userNotFound);
      }
    }
  };

  return (
    <form className='form-github-search-container'>
      <fieldset>
        <Label
          id="github-name"
          placeholder="digite o nome do usuário"
          text="Buscar repositório no github"
          value={ username }
          handleChange={ ({ target }) => setUsername(target.value) }
        />
        <Button
          icon={ <GoSearch />}
          text='Pesquisar'
          onClick={ handleClick }
        />
      </fieldset>
    </form>
  );
};  

export default Search;