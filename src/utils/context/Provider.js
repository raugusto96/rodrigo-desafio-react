import React, { useState } from 'react';
import propTypes from 'prop-types';
import UserContext from './UserContext';

const Provider = ({ children }) => {
  const [user, setUser] = useState({});
  const contextValue = {
    user,
    setUser
  };

  return (
    <UserContext.Provider value={ contextValue }>
      { children }
    </UserContext.Provider>
  );
};

Provider.propTypes = {
  children: propTypes.node,
}.isRequired;

export default Provider