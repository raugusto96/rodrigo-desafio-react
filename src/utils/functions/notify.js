import { toast } from 'react-toastify';

export default (errrorMessage) => toast.error(errrorMessage, {
  position: 'top-right',
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
  theme: 'dark',
});