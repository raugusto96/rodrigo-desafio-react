const saveItem = (key, value) => {
  const itemInStorage = JSON.parse(localStorage.getItem(key));
  if (itemInStorage === null) {
    localStorage.setItem(key, JSON.stringify(value));
  } else {
    localStorage.setItem(key, JSON.stringify(itemInStorage))
  }
};

const recoveryItem = (key) => {
  return JSON.parse(localStorage.getItem(key));
}

const clearStorage = () => localStorage.clear();

export default { saveItem, recoveryItem, clearStorage };