const errorsMessage = {
  usernameNotDefined: '❌ Informe um nome de usuário válido do github!',
  userNotFound: '❌ Usuário não encontrado no github. Verifique se você digitou o nome corretamente!'
};

export default errorsMessage;