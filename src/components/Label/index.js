import React from 'react';
import propTypes from 'prop-types';

function Label({ id, text, placeholder, value, handleChange }) {
  return (
    <label htmlFor={ id }>
      { text }
      <input
        autoComplete='off'
        type="text"
        id={ id }
        placeholder={ placeholder }
        value={ value }
        onChange={ handleChange }
      />
    </label>
  );
}

Label.propTypes = {
  id: propTypes.string,
  text: propTypes.string,
  placeholder: propTypes.string,
  value: propTypes.string,
  handleChange: propTypes.func,
}.isRequired;

export default Label;