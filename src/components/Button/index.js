import React from 'react';
import propTypes from 'prop-types';

const Button = ({ onClick, text, icon }) => {
  return (
    <button
      type="button"
      onClick={ onClick }
    >
      { icon }
      { text }
    </button>
  );
};

Button.propTypes = {
  text: propTypes.string,
}.isRequired;

Button.defaultProps = {
  icon: null,
  onClick: () => {},
}

export default Button;