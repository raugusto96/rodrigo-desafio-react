import React, { useEffect, useState } from 'react';
import moment from 'moment';
import propTypes from 'prop-types';
import api from '../../api';
import { BiGitRepoForked } from 'react-icons/bi';
import { FaBalanceScale } from 'react-icons/fa';

const Card = ({ title, bio, language, updated, owner, forks, license }) => {
  const [repositoryData, setRepositoryData] = useState({});
  const date = moment(updated).format("D MMM");

  const fetchRepository = async () => {
    try {
      const response = await api.getRepositoryData(owner, title);
      setRepositoryData(response);
    } catch (error) {
      console.log(error.message);
    }
  };
  
  useEffect(() => {
    fetchRepository();
  }, []);

  return (
    <div className='card-container'>
      <h2>{ title }</h2>
      { repositoryData.parent && (<p>Forked from { repositoryData.parent.full_name }</p>) }
      { bio && (<p>{ bio }</p>) }
      <div>
        <div className="repo-language-color" id={ language ? language : 'JavaScript' } />
        <span>{ language ? language : 'JavaScript' }</span>
        { forks > 0 && (<span>{ <BiGitRepoForked /> } { forks }</span>) }
        { license && (<span>{ <FaBalanceScale /> } { license }</span>) }
        <relative-time datetime={ updated } >Updated on { date }</relative-time>
      </div>

    </div>
  );
};

Card.propTypes = {
  title: propTypes.string,
  language: propTypes.string,
  updated: propTypes.string,
}.isRequired;

export default Card;