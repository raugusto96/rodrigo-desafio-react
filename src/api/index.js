import axios from 'axios';

const getUserData = async (username) => {  
  const URL = `https://api.github.com/users/${username}`;
  const { data } = await axios.get(URL, {
    headers: {
      'Authorization': process.env.REACT_APP_API_TOKEN,
    }
  });
  return data;
};

const getRepositoriesData = async (username) => {
  const URL = `https://api.github.com/users/${username}/repos`;
  const { data } = await axios.get(URL, {
    headers: {
      'Authorization': process.env.REACT_APP_API_TOKEN,
    }
  });
  return data;
};

const getRepositoryData = async (username, name) => {
  const URL = `https://api.github.com/repos/${username}/${name}`;
  const { data } = await axios.get(URL, {
    headers: {
      'Authorization': process.env.REACT_APP_API_TOKEN,
    }
  });
  return data;
}

export default { getUserData, getRepositoriesData, getRepositoryData };