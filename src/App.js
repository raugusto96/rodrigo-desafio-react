import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Search from './pages/Search';
import Profile from './pages/Profile'
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <Routes>
      <Route path="/" element={ <Search /> }/>
      <Route path="/profile" element={ <Profile /> }/>
    </Routes>
  );
}

export default App;